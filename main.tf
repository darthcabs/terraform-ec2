provider "aws" {
  region = "us-east-1"
}

##############
## Instance ##
##############
data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

resource "aws_instance" "ec2" {
  ami = "${data.aws_ami.ubuntu.id}"
  associate_public_ip_address = true
  instance_type = "t3.micro"
  key_name = "ec2-key"
  vpc_security_group_ids = ["${aws_security_group.secgroup.id}"] 
  
  tags = {
    Name = "Agility"
  }

  # Install Docker and Java
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt install -y docker.io",
      "sudo usermod -aG docker ubuntu"
    ]

    connection {
      type     = "ssh"
      user     = "ubuntu"
      host     = "${aws_instance.ec2.public_ip}"
      private_key = "${file("${var.key_pair_path}")}"
    }
  }

  # Run the Pets web page
  provisioner "remote-exec" {
    inline = [
      "docker run -d -p 5000:5000 -p 7000:7000 registry.gitlab.com/darthcabs/docker-pets:latest"
    ]

    connection {
      type     = "ssh"
      user     = "ubuntu"
      host     = "${aws_instance.ec2.public_ip}"
      private_key = "${file("${var.key_pair_path}")}"
    }
  }
}

####################
## Security Group ##
####################

resource "aws_security_group" "secgroup" {
  name        = "agility"
  description = "Allow access to ports 22 and 80"

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Group"
  }
}